package com.ats.datapegawaii.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_PEGAWAI")
public class PegawaiModel {

	@Id
	@Column(name="CLM_NO")
	private int No;
	
	@Column(name="CLM_NP")
	private String namaPegawai;
	
	@Column(name="CLM_GP")
	private int gajiPegawai;
	
	@Column(name="CLM_STS")
	private String Status;
	
	public int getNo() {
		return No;
	}
	public void setNo(int no) {
		No = no;
	}
	public String getNamaPegawai() {
		return namaPegawai;
	}
	public void setNamaPegawai(String namaPegawai) {
		this.namaPegawai = namaPegawai;
	}
	public int getGajiPegawai() {
		return gajiPegawai;
	}
	public void setGajiPegawai(int gajiPegawai) {
		this.gajiPegawai = gajiPegawai;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
}
