package com.ats.datapegawaii;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatapegawaiiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatapegawaiiApplication.class, args);
	}

}
